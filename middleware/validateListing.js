const jsonschema = require("jsonschema");

const newListingSchema = require("../schemas/newListingSchema.json");
const { BadRequestError } = require("../server/expressErrors");
/**
 * validateNewListing Middleware
 *
 * Required Properties:
 * *  address      : `String`,
 * *  ageInDays    : `Number`,
 * *  isDeleted    : `Boolean`,
 * *  propertyType : `String`
 *
 * Throws `400` error if missing properties, or wrong type
 */
const validateNewListing = (req, res, next) => {
  try {
    const validator = jsonschema.validate(req.body, newListingSchema);

    if (!validator.valid) {
      const errors = validator.errors.map((e) => e.stack);
      throw new BadRequestError(errors);
    }

    return next();
  } catch (error) {
    return next(error);
  }
};
/**
 * validateListingUpdate Middleware
 *
 * Throws `400` error if `req.body` does not have any properties or
 * if `_id` is a property.
 */
const validateListingUpdate = (req, res, next) => {
  try {
    const properties = Object.keys(req.body);
    const errors = [];

    if (!properties.length) {
      errors.push("Please provide properties to update.");
    }

    if (req.body.hasOwnProperty("_id")) {
      errors.push("_id can not be modified.");
    }

    if (errors.length) {
      throw new BadRequestError(errors);
    }

    return next();
  } catch (error) {
    return next(error);
  }
};

module.exports = { validateNewListing, validateListingUpdate };
