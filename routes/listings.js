const express = require("express");
const router = new express.Router();

const Listing = require("../models/listing");
const {
  validateNewListing,
  validateListingUpdate,
} = require("../middleware/validateListing");

/**
 * POST / { listing } => { listing }
 */
router.post("/", validateNewListing, async (req, res, next) => {
  try {
    const listing = Listing.create(req.body);

    return res.status(201).json({ listing });
  } catch (error) {
    return next(error);
  }
});

/**
 * GET / => { listings : [] }
 * - Limited to 20
 * - Excluding delisted listings
 */
router.get("/", async (req, res, next) => {
  try {
    const listings = await Listings.getAll();

    return res.json({ listings });
  } catch (error) {
    return next(error);
  }
});

/**
 * GET /:_id => { listing }
 */
router.get("/:_id", async (req, res, next) => {
  try {
    const { _id } = req.params;
    const listing = await Listing.getById(_id);

    return res.json({ listing });
  } catch (error) {
    return next(error);
  }
});

/**
 * PATCH /:_id
 */
router.patch("/:_id", validateListingUpdate, async (req, res, next) => {
  try {
    const { _id } = req.params;
    const listing = await Listing.update(_id, req.body);

    return res.json({ listing });
  } catch (error) {
    return next(error);
  }
});

/**
 * DELETE /:_id
 */
router.delete("/:_id", async (req, res, next) => {
  try {
    const { _id } = req.params;
    const listing = await Listing.remove(_id);

    return res.json({ listing });
  } catch (error) {
    return next(error);
  }
});

module.exports = router;
