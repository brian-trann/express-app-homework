const client = require("../server/client");
const { NotFoundError } = require("../server/expressErrors");
class Listing {
  static INDEX = "listing";
  /**
   * Create a listing from data
   */
  static async create(body) {
    const response = await client.create({
      index: Listing.INDEX,
      body,
    });

    return response;
  }

  /**
   * Get All
   */
  static async getAll(from = 0) {
    const { body } = await client.search({
      index: Listing.INDEX,
      from,
      size: 20,
      body: { query: { match: { isDeleted: false } } },
    });

    return { listings: body.hits.hits };
  }

  /**
   * Get by ID
   */
  static async getById(_id) {
    const response = await client.get({
      index: Listing.INDEX,
      _id,
    });

    if (!response.found) throw new NotFoundError(`No Listing with ID: ${_id}`);

    return response;
  }
  /**
   * Update
   */
  static async update(_id, doc) {
    await client.update({
      index: Listing.INDEX,
      body: { doc },
      _id,
    });
    const listing = await Listing.getById(_id);

    return listing;
  }

  /**
   * "DELETE" a listing. This marks a listing as deleted, but does not remove from db
   */
  static async remove(_id) {
    await client.update({
      index: Listing.INDEX,
      body: { doc: { isDeleted: true } },
      _id,
    });
    const listing = await Listing.getById(_id);

    return listing;
  }
}

module.exports = Listing;
