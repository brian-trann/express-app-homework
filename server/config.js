require("dotenv").config();

const PORT = +process.env.PORT || 3001;
const USERNAME = process.env.USERNAME;
const PASSWORD = process.env.PASSWORD;
const CLOUD_ID = process.env.CLOUD_ID;

module.exports = { PORT, USERNAME, PASSWORD, CLOUD_ID };
