const { Client } = require("@elastic/elasticsearch");
const { USERNAME, PASSWORD, CLOUD_ID } = require("./config");

const client = new Client({
  cloud: {
    id: CLOUD_ID,
  },
  auth: {
    username: USERNAME,
    password: PASSWORD,
  },
});

module.exports = client;
