const express = require("express");

const app = express();

const { NotFoundError } = require("./expressErrors");

const listingsRoutes = require("../routes/listings");

app.use(express.json());

app.use("/listings", listingsRoutes);

/** Handle 404 errors */
app.use((req, res, next) => {
  return next(new NotFoundError());
});

/** Generic error handler */
app.use((err, req, res, next) => {
  const status = err.status || 500;
  const message = err.message;

  return res.status(status).json({ error: message, status });
});

module.exports = app;
