/**
 * ExpressError extends normal JS error, so we can add a status and message
 * when we make an instance of it
 */
class ExpressError extends Error {
  constructor(message, status) {
    super();
    this.message = message;
    this.status = status;
  }
}
/** 404 Not Found */
class NotFoundError extends ExpressError {
  constructor(message = "Not Found") {
    super(message, 404);
  }
}
/** 400 BadRequest */
class BadRequestError extends ExpressError {
  constructor(message = "Bad Request") {
    super(message, 400);
  }
}

module.exports = { ExpressError, NotFoundError, BadRequestError };
