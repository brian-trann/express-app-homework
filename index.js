const app = require("./server/app");

const { PORT } = require("./server/config");

app.listen(PORT, () => {
  console.log(`Started on PORT: ${PORT}`);
});
